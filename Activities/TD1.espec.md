## TD1: Setup

1. Download and Install **git** and **RStudio** (take the free version)
   - Git: https://git-scm.com/downloads
     - _Understand git and Tutorial_: https://try.github.io/
   - RStudio: https://www.rstudio.com/products/rstudio/download/
     for mac users you need to download R https://cran.r-project.org/

2. Login to your Gitlab account
   - Go to the login page: https://gricad-gitlab.univ-grenoble-alpes.fr
   - Using UGA Agalan login/password
   - _Documentation_: Have a look at the Gitlab documentation https://docs.gitlab.com/ee/gitlab-basics/README.html

3. Add your public SSH key
   - Add it here: https://gricad-gitlab.univ-grenoble-alpes.fr/profile/keys
   - _Tutorial_: https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html

4. Fork the reference repository
   - Fork this repository: https://gricad-gitlab.univ-grenoble-alpes.fr/MSPL/mspl-2018-2019
     - Please, use the `Fork` button in the web interface
   - _Tutorial_: https://docs.gitlab.com/ee/gitlab-basics/fork-project.html

5. Clone your forked repository locally
   - `git clone git@gricad-gitlab.univ-grenoble-alpes.fr:YOURLOGIN/mspl-2018-2019.git`
   - _Tutorial_: https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html


6. Create your first commit, then push it to gitlab
   - Configure git:
```
git config --global user.name "MY NAME"
git config --global user.email "MY EMAIL"
```
   - Create a file `AUTHOR.txt` with your name
   - Then, run `git add AUTHOR.txt`
   - Commit it locally using `git commit -m "my first commit"`
   - Push to github with `git push origin master`

7. Add mspl-2018-2019 remote

   Do in your local repository, the one that you've cloned, the following commands:
   - Run `git remote add mspl-2018-2019 git@gricad-gitlab.univ-grenoble-alpes.fr:MSPL/mspl-2018-2019.git`
   - Check for update and (merge or rebase) them locally by : `git pull mspl-2018-2019 master`
        for other choices have a look at fetch, merge and rebase commands
   - Push then to your gitlab repository: `git push origin master`

   Repeat the last two commands to keep your gitlab repository updated.

   This is necessary to keep yourself up to date with latest TD updates.

Congratulations, you're done for the TD1.
